# -*- encoding: utf-8 -*-

import socket
import sys

def input(first):
    while True:
        tmp=""
        if first:
            print("Podaj pierwszą liczbę.")
        else:
            print("Podaj drugą liczbę.")
        tmp=sys.stdin.readline()
        try:
            number=float(tmp)
            break
        except:
            print("Coś nie tak.")
            pass
    return number

SumSocket=socket.socket()

ServerAddress=('194.29.175.240', 31007)

SumSocket.connect(ServerAddress)

try:
    n1=input(True)
    n2=input(False)
    print("Wysyłam zapytanie o "+str(n1)+"+"+str(n2)+".")

    SumSocket.send((str(n1)+"+"+str(n2)+" ").encode('utf-8'))
    
    response=""
    data=SumSocket.recv(1)
    while data!=b'':
        response=response+data.decode('utf-8')
        data=SumSocket.recv(1)

    print(response)
    pass

finally:
    SumSocket.close()
    pass
