# -*- encoding: utf-8 -*-

import socket

#licznik =0
COUNTER = 0

# Tworzenie gniazda TCP/IP
SOCKET= socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_IP)

# Powiązanie gniazda z adresem
server_address = ('194.29.175.240', 31007)  # TODO: zmienić port!

# Nasłuchiwanie przychodzących połączeń
SOCKET.bind(server_address)
SOCKET.listen(1)

while True:
    # Czekanie na połączenie
    conn, addr = SOCKET.accept()

    # Podbicie licznika
    COUNTER=COUNTER+1;

    try:
        # Wysłanie wartości licznika do klienta
        conn.send(str(COUNTER))
        pass

    finally:
        # Zamknięcie połączenia
        conn.close();
        pass
